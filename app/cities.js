const express = require('express');
const router = express.Router();
const City = require('./models/City');
const auth = require('./middleware/auth');

const createRouter = () =>{
    router.post('/', auth, async (req, res) => {         
        const city = new City(req.body);

        try{
            await city.save();
            res.send({message: "City successfully added"});
        } catch (e) {
            res.status(500).send(e);
        }
    });

    router.get('/', async (req, res) => {
        try{
            cities = await City.find();
            res.send(cities);
        } catch (e) {
            res.status(500).send(e);
        }
    });

    router.delete('/:id', auth, async (req, res) => {
        try {
            const city = await City.findOne({_id: req.params.id});

            if(!city) {
                return res.status(401).send({error: "This city is not exist"});
            }

            await City.findByIdAndRemove({_id: req.params.id}, function (e, docs) { 
                if (e){ 
                    console.log(e);
                    return res.status(403).send({error: "City cannot be deleted"});
                } 
            });
            res.send({message: "City removed successfully"});
        } catch (e) {
            res.status(500).send(e);
        }
    });
    return router;
};

module.exports = createRouter;