const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const TariffSchema = new Schema({
    locationFrom: {
        type: Schema.Types.ObjectId,
        ref: 'City',
        required: true
    },
    locationTo: {
        type: Schema.Types.ObjectId,
        ref: 'City',
        required: true
        },
    tariff: {
        type: Number,
        required: true,
    },
    }, {versionKey: false});

const Tariff = mongoose.model('Tariff', TariffSchema);

module.exports = Tariff;