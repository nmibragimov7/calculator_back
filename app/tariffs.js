const express = require('express');
const router = express.Router();
const Tariff = require('./models/Tariff');
const auth = require('./middleware/auth');

const createRouter = () =>{
    router.post('/', auth, async (req, res) => {    
        const tariff = new Tariff(req.body);

        try{
            await tariff.save();
            res.send({message: "Tariff successfully added"});
        } catch (e) {
            res.status(500).send(e);
        }
    });

    router.post('/calculate', auth, async (req, res) => {    
        try{
            const {locationFrom, locationTo, weight} = req.body;
            Tariff.findOne({
                $and: [
                    {locationFrom: locationFrom}, 
                    {locationTo: locationTo}
                ]
            }).then((tariff)=>{
                if(!tariff) return res.status(404).send({error: "Tariff does not found"});
                const price = (weight - 20)*tariff.tariff + 7000;
                return res.send({tariff: price});
            });
        } catch (e) {
            res.status(500).send(e);
        }
    });

    router.get('/', async (req, res) => {
        try{
            tariffs = await Tariff.find().populate({path: "locationFrom"}).populate({path: "locationTo"});
            res.send(tariffs);
        } catch (e) {
            res.status(500).send(e);
        }
    });

    router.delete('/:id', auth, async (req, res) => {
        try {
            const tariff = await Tariff.findOne({_id: req.params.id});

            if(!tariff) {
                return res.status(401).send({error: "This tariff is not exist"});
            }

            await Tariff.findByIdAndRemove({_id: req.params.id}, function (e, docs) { 
                if (e){ 
                    console.log(e);
                    return res.status(403).send({error: "Tariff cannot be deleted"});
                } 
            });
            res.send({message: "Tariff removed successfully"});
        } catch (e) {
            res.status(500).send(e);
        }
    });
    return router;
};

module.exports = createRouter;