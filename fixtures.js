const mongoose = require('mongoose');
const config = require('./app/config');
const User = require('./app/models/User');
const City = require('./app/models/City');
const Tariff = require('./app/models/Tariff');
const {nanoid} = require('nanoid');

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

db.once('open', async () => {
    try {
        await db.dropCollection('users');
        await db.dropCollection('cities');
        await db.dropCollection('tariffs');
    } catch (e) {
        console.log('Collection not found. Drop collections skiped...');
    }

    await User.create({
        username: "admin",
        email: "admin@admin.com",
        displayName: "Admin1",
        phone: 87012223311,
        password: "admin",
        token: nanoid()
    });

    const [city1, city2, city3, city4] = await City.create({
        title: "Алматы",
    }, {
        title: "Нур-Султан",
    }, {
        title: "Караганда",
    }, {
        title: "Шымкент",
    });

    await Tariff.create({
        locationFrom: city1._id,
        locationTo: city2._id,
        tariff: 200,
    }, {
        locationFrom: city1._id,
        locationTo: city3._id,
        tariff: 120,
    }, {
        locationFrom: city1._id,
        locationTo: city4._id,
        tariff: 100,
    }, {
        locationFrom: city2._id,
        locationTo: city1._id,
        tariff: 200,
    }, {
        locationFrom: city2._id,
        locationTo: city3._id,
        tariff: 80,
    }, {
        locationFrom: city2._id,
        locationTo: city4._id,
        tariff: 300,
    }, {
        locationFrom: city3._id,
        locationTo: city1._id,
        tariff: 120,
    }, {
        locationFrom: city3._id,
        locationTo: city2._id,
        tariff: 80,
    }, {
        locationFrom: city3._id,
        locationTo: city4._id,
        tariff: 220,
    }, {
        locationFrom: city4._id,
        locationTo: city1._id,
        tariff: 100,
    }, {
        locationFrom: city4._id,
        locationTo: city2._id,
        tariff: 300,
    }, {
        locationFrom: city4._id,
        locationTo: city3._id,
        tariff: 220,
    });

   await db.close();
});